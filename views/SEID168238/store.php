<?php

require_once ("../../vendor/autoload.php");

$objStudent = new \App\Student();
$objCourse = new \App\Course();

$objStudent->setData($_POST);
$objCourse->setData($_POST);

$studentArr = $objStudent->getData();
$courseArr = $objCourse->getData();


$name = $studentArr["name"];
$session = $studentArr["session"];
$studentID = $studentArr["studentID"];
$php = $courseArr["php"];
$java = $courseArr["java"];
$dot_net = $courseArr["dot_net"];
$design = $courseArr["design"];


$data2write = <<<BITM
*************************************
NAME: $name
SESSION: $session
StudentID: $studentID
=====================================
SUBJECT         MARKS         GRADE
*************************************

BITM;

file_put_contents("result.txt",$data2write,FILE_APPEND);

file_put_contents("result.txt","PHP"."             ".$php. printSpace(strlen($php)) .mark2Grade($php) ."\n",FILE_APPEND);
file_put_contents("result.txt","Java"."            ".$java. printSpace(strlen($java)).mark2Grade($java) ."\n",FILE_APPEND);
file_put_contents("result.txt","Dot Net"."         ".$dot_net. printSpace(strlen($dot_net)) .mark2Grade($dot_net)."\n",FILE_APPEND);
file_put_contents("result.txt","Design"."          ".$design.printSpace(strlen($design)).mark2Grade($design) ."\n",FILE_APPEND);




function printSpace($len){
    $space = "";
    for($i=1; $i<= 14 - $len; $i++)
        $space .= " ";

    return $space;
}


function mark2Grade($mark){

    if($mark>=80) return "A+";
    else if($mark>=70) return "A";
    else if($mark>=60) return "A-";
    else if($mark>=50) return "B";
    else if($mark>=40) return "C";
    else return "F";

}


header("Location: view.php");

