<!doctype html>
<html lang="en">
<body>
<div class="container">

    <div>
        <h1 align="center">Student Info</h1>

        <form action="store.php" method="post">


            <div class="form-group">
                <label for="Name">Name</label>
                <input type="text" class="form-control" id="name" name="Name" placeholder="Enter Your Name">
            </div>

            <div class="form-group">
                <label for="Session">Session</label>
                <input type="text" class="form-control" id="session" name="Session" placeholder="Enter Session">
            </div>

            <div class="form-group">
                <label for="studentID">StudentID</label>
                <input type="text" class="form-control" id="studentID" name="StudentID" placeholder="Enter Student ID">
            </div>



            <h1 align="center">MARKS OBTAINED</h1>
            <div class="form-group">
                <label for="php">PHP</label>
                <input type="text" class="form-control" id="php" name="PHP" placeholder="Enter Marks">
            </div>

            <div class="form-group">
                <label for="java">Java</label>
                <input type="text" class="form-control" id="java" name="Java" placeholder="Enter Marks">
            </div>

            <div class="form-group">
                <label for="dot net">Dot Net</label>
                <input type="text" class="form-control" id="dot net" name="DotNet" placeholder="Enter Marks">
            </div>

            <div class="form-group">
                <label for="design">Design</label>
                <input type="text" class="form-control" id="design" name="Design" placeholder="Enter Marks">
            </div>

            <button type="submit" class="btn btn-default">Submit</button>

        </form>
    </div>

</div>
</body>
</html>