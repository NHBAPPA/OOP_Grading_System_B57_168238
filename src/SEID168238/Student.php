<?php

namespace App;


class Student
{
    public $name;

    public $session;

    public $studentID;


    public function setData($postDataArray)
    {

        if(array_key_exists("Name",$postDataArray)) {
            $this->name = $postDataArray["Name"];
        }

        if(array_key_exists("Session",$postDataArray)) {
            $this->session = $postDataArray["Session"];
        }

        if(array_key_exists("StudentID",$postDataArray)) {
            $this->studentID = $postDataArray["StudentID"];
        }
    }


    public function getData(){

        $name = $this->name;

        $session = $this->session;

        $studentID = $this->studentID;

        $varList = array("name","session","studentID");
        $studentInfoArray  =  compact($varList);

        return $studentInfoArray;


    }
}

