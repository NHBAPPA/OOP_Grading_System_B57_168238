<?php


namespace App;


class Course
{
    public $php;
    public $java;
    public $dot_net;
    public $design;

    public function setData($postDataArray)
    {
        if (array_key_exists("PHP", $postDataArray))
        {
            $this->php = $postDataArray['PHP'];
        }
        if (array_key_exists("Java", $postDataArray))
        {
            $this->java = $postDataArray['Java'];
        }
        if (array_key_exists("DotNet", $postDataArray))
        {
            $this->dot_net = $postDataArray['DotNet'];
        }
        if (array_key_exists("Design", $postDataArray))
        {
            $this->design = $postDataArray['Design'];
        }
    }

    public function getData(){

        $php = $this->php;
        $java = $this->java;
        $dot_net = $this->dot_net;
        $design = $this->design;

        $varList = array("php","java","dot_net","design");
        $courseInfoArray  =  compact($varList);

        return $courseInfoArray;


    }
}